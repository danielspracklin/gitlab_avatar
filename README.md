# gitlab_avatar

This is a quick R script that creates a GitLab avatar. It uses the [tidyverse](https://www.tidyverse.org) to create and visualize a tibble in polar coordinates, then applies a discrete [viridis colour palette](https://cran.r-project.org/web/packages/viridis/viridis.pdf) to the resulting plot.
